# OpenML dataset: Traffic-Violations-in-USA

https://www.openml.org/d/43524

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Traffic violations followed the invention of the automobile: the first traffic ticket in the United States was allegedly given to a New York City cab driver on May 20, 1899, for going at the breakneck speed of 12 miles per hour. Since that time, countless citations have been issued for traffic violations across the country, and states have reaped untold billions of dollars of revenue from violators.
Traffic violations are generally divided into major and minor types of violations. The most minor type are parking violations, which are not counted against a driving record, though a person can be arrested for unpaid violations.
The most common type of traffic violation is a speed limit violation. Speed limits are defined by state.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43524) of an [OpenML dataset](https://www.openml.org/d/43524). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43524/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43524/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43524/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

